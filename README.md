# Devoxx4Kids



[mBot](https://gitlab.com/craftsmen/devoxx4kids/-/tree/master/mBot) 
- Opdracht 1 | [Richtingaanwijzers maken](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/01_Richtingaanwijzers_Maken.pdf) | [Uitgewerkte code](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/01_Richtingaanwijzers_Maken.sb2)
- Opdracht 2 | [Jij bestuurt de robot](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/02_Jij_Bestuurt_de_Robot.pdf) | [Uitgewerkte code](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/02_Jij_Bestuurt_de_Robot.sb2)
- Opdracht 3 | [De robot kan zelf sturen!](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/03_De_Robot_Kan_Zelf_Sturen.pdf) | [Uitgewerkte code](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/mBot/03_De_Robot_Kan_Zelf_Sturen.sb2)

[AppInventor](https://gitlab.com/craftsmen/devoxx4kids/-/tree/master/AppInventor2023)
- Opdracht | [Maak je eigen spel](https://gitlab.com/craftsmen/devoxx4kids/-/blob/master/AppInventor2023/KadasterCraftsmen_Devoxx4Kids_Mario_v1.0.pdf)
